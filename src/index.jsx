import { render, h } from 'preact'
import { html } from 'htm/preact';
import router from './services/router'
import Header from './components/header';
import deletedItems from './services/deletedItems'

render(<Header />, document.getElementById("header"));



router.on('/home', function (params) {
  import('./App.jsx')
    .then(App => {
      let Home = App.default
      render(html`<${Home} />`, document.getElementById("root"));
    });
  document.getElementById("trash").style.display = "none";
  document.getElementById("root").style.display = "";
})

router.on('/trash', function (params) {
  import('./Trash.jsx')
    .then(Bin => {
      let Trash = Bin.default
      render(html`<${Trash} />`, document.getElementById("trash"));
    });
  document.getElementById("root").style.display = "none";
  document.getElementById("trash").style.display = "";
})
router.emit('/home')


import Freezer from 'freezer-js'
const freezer = new Freezer({
  todos: []
});
export default freezer
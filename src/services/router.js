import nanorouter from 'nanorouter'
const router = nanorouter({ default: '/404' })

export default router

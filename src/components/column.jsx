import { h, Component } from 'preact';
import { html } from 'htm/preact';
import styled from 'styled-components';
import Card from './card';
const column = props => {
    let col1 = props.items.filter(item => item.group === 1)
    let col2 = props.items.filter(item => item.group === 2)
    let col3 = props.items.filter(item => item.group === 3)
    let col4 = props.items.filter(item => item.group === 4)
    return (
        html`
        <${Container}>
        <${Heading}>${props.name}</${Heading}>
        ${props.name == "To Do" ? col1.map(item => {
            return html`<${Card} delete=${props.delete} increase=${props.increase} text=${item.text} id=${item.id}/>`
        })
                : props.name == "In Progress" ? col2.map(item => {
                    return html`<${Card} decrease=${props.decrease} increase=${props.increase}text=${item.text} id=${item.id}/>`
                })
                    : props.name == "Completed" ? col3.map(item => {
                        return html`<${Card} decrease=${props.decrease} delete=${props.delete} text=${item.text} id=${item.id}/>`
                    })
                        : props.name == "Trash" ? col4.map(item => {
                            return html`<${Card} delete=${props.delete} name=${props.name} text=${item.text} id=${item.id}/>`
                        }) : null
            }
        </${Container}> `

    );
};
const Container = styled.div`
display: flex;
flex-direction: column;
height: 800px;
width: 27vw;
background-color: grey;
padding: 0 20px;
margin: 5px 20px;
`;
const Heading = styled.p`
text-align: center;
font-weight: 500;
font-size: 2.5rem;
`;
export default column;
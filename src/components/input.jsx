import { h } from 'preact';
import { html } from 'htm/preact';
import styled from 'styled-components'
import { useState } from 'preact/hooks';
import freezer from '../services/store';

const Input = () => {
  const [fieldInput, setFieldInput] = useState('');
  const handleChange = (e) => {
    setFieldInput(e.target.value);
  }
  const handleClick = (e) => {
    freezer.get().todos.push({ text: fieldInput, group: 1, id: Math.random().toString() }).now();
    setFieldInput('');
  }
  return (
    html`
        <${Container}>          
            <${Field} type="input" placeholder="Enter Item" value=${fieldInput} onChange=${handleChange} />
            <${Button} type="button"  onClick=${handleClick}>ADD ITEM</${Button}>
        </${Container}>
        `
  );
};

const Field = styled.input`
  border: 1px solid black;
  width:400px;
  height:50px;
  font-size: inherit;
  background-color: transparent;
  border-radius:5px;
  padding-left: 5px;
  &:focus {
    outline: none;
  }
`;
const Button = styled.button`
width:100px;
height:50px;
border-radius:5px;
margin-left:5px;
`;
const Container = styled.div`
display:flex;
width:100vw;
margin-top:11vh;
justify-content:center;
align-items:center;
`;
export default Input;
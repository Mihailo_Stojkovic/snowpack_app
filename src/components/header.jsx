import { h, Component } from 'preact';
import { html } from 'htm/preact';
import styled from 'styled-components';
import { useEffect } from 'preact/hooks';
import router from '../services/router';
import input from './input';
const Header = () => {
    useEffect(() => {
        document.querySelectorAll("a").forEach((e) => {
            e.addEventListener("click", (event) => {
                event.preventDefault();
                if (event.target.innerText === "TRASH") {
                    router.emit('/trash')
                } else if (event.target.innerText === "HOME") {
                    router.emit('/home')
                }
            });
        });
    }, []);

    return (html`
        <${Container}>
            <${Lista}>
                <${Item}><a>HOME</a></${Item}>
                <${Item}><a>TRASH</a></${Item}>
            </${Lista}>            
        </${Container}>
        <${input}/>
        `
    );
};


const Item = styled.div`
padding:0 20px;
cursor:pointer;
font-weight:700;
font-size:3.5rem;
`
const Container = styled.div`
position:absolute;
top:0;
left:0;
width:100vw;
height:10vh;
background-color:grey;
color:white;
`
const Lista = styled.div`
height:100%;
list-style-type: none;
display:flex;
justify-content:center;
align-items:center;
`
export default Header;
import { h } from 'preact';
import { html } from 'htm/preact';
import styled from 'styled-components';

const card = (props) => {
    return (
        html`
        <${Container}>        
            ${props.name === 'Trash' ? html`<${button3} />` :
                props.delete && props.increase ? html`<${button} onClick=${() => props.delete(props.id)}/>`
                    : html`<${button2} onClick=${() => props.decrease(props.id)} />`
            }
<${p}>${props.text}</${p}>
    ${props.increase ? html`<${button2} onClick=${() => props.increase(props.id)}/>` : html`<${button} onClick=${() => props.delete(props.id)}/>`}
        </${Container}> `
    );
};
const button = styled.button`
cursor: pointer;
background-color:red;
width:20%;
height:50px;
border-radius:5px;
transition: all .2s ease-in-out;
&:hover{
    transform:scale(1.1);
}
`;
const button2 = styled(button)`
background-color:green;
`;
const button3 = styled(button)`
visibility: hidden;
`;

const p = styled.span`
width:60%;
height: 50px;
text-align:center;
font-size: 2.5em;
`;
const Container = styled.div`
display: flex;
width: 27vw;
background-color:white;
height:50px;
margin-bottom:10px;
border-radius:5px;
`;
export default card;
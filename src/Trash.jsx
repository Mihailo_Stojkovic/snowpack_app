import { h } from 'preact'
import { useEffect } from 'preact/hooks';
import { html } from 'htm/preact';
import column from './components/column';
import styled from 'styled-components';
import Asa from './services/deletedItems';
import freezer from './services/store'
import Emitter from './services/EventEmitter';
import { decTo5 } from './services/deletedItems'
function Trash() {
  useEffect(() => {
    const me = this;
    freezer.on('update', function () {
      me.forceUpdate()
    });
  }, [])

  let newArray = Asa()
  let zbx = decTo5()
  const deleteFromTrash = (id) => {
    let decreased = freezer.get().todos.find((num) => num.id === id).set({ group: 5 }).now()
    return Emitter.emit("to5", decreased)
  }
  return (html`
    <div className="Trash">
      <${wrapper}>
      <${column} name="Trash" delete=${deleteFromTrash} items=${freezer.get().todos}/>
      </${wrapper}>
    </div>`
  );
}
const wrapper = styled.div`
display:flex;
flex-direction:row;
justify-content:center;
align-items:center;
`;
export default Trash;
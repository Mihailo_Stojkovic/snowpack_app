import { h } from 'preact';
import { useEffect } from 'preact/hooks';
import { html } from 'htm/preact';
import column from './components/column';
import styled from 'styled-components';
import Emitter from './services/EventEmitter';
import freezer from './services/store'
import { decreased, decTo1, decTo3 } from './services/deletedItems'
function App() {
  useEffect(() => {
    const me = this;
    freezer.on('update', function () {
      me.forceUpdate()
    });
  }, [])
  let decreasedItems = decreased()
  let decto1 = decTo1();
  let decto3 = decTo3()
  const deleteHandler = (id) => {
    let deleted = freezer.get().todos.find((num) => num.id === id).set({ group: 4 }).now()
    return Emitter.emit("deleted", deleted)
  }
  const decreaseHandler = (id) => {
    let decreased = freezer.get().todos.find((num) => num.id === id).set({ group: 2 }).now()
    return Emitter.emit("deacreased", decreased)
  }
  const decreaseHandlerTo1 = (id) => {
    let decreased = freezer.get().todos.find((num) => num.id === id).set({ group: 1 }).now()
    return Emitter.emit("deacreased", decreased)
  }
  const decreaseHandlerTo3 = (id) => {
    let decreased = freezer.get().todos.find((num) => num.id === id).set({ group: 3 }).now()
    return Emitter.emit("to3", decreased)
  }

  return (html`
    <div className="App">
      <${wrapper}>
      <${column} name="To Do" delete=${deleteHandler} increase=${decreaseHandler} items=${freezer.get().todos}/>
      <${column} name="In Progress" decrease=${decreaseHandlerTo1} increase=${decreaseHandlerTo3}items=${freezer.get().todos}/>
      <${column} name="Completed" decrease=${decreaseHandler} delete=${deleteHandler} items=${freezer.get().todos}/>
      </${wrapper}>
    </div>`
  );
}
const wrapper = styled.div`
display:flex;
`;
export default App;